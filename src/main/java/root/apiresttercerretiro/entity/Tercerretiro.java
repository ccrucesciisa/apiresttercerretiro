/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package root.apiresttercerretiro.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author ccruces
 */
@Entity
@Table(name = "tercerretiro")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Tercerretiro.findAll", query = "SELECT t FROM Tercerretiro t"),
    @NamedQuery(name = "Tercerretiro.findByRut", query = "SELECT t FROM Tercerretiro t WHERE t.rut = :rut"),
    @NamedQuery(name = "Tercerretiro.findByPorcentaje", query = "SELECT t FROM Tercerretiro t WHERE t.porcentaje = :porcentaje")})
public class Tercerretiro implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2147483647)
    @Column(name = "rut")
    private String rut;
    @Size(max = 2147483647)
    @Column(name = "porcentaje")
    private String porcentaje;

    public Tercerretiro() {
    }

    public Tercerretiro(String rut) {
        this.rut = rut;
    }

    public String getRut() {
        return rut;
    }

    public void setRut(String rut) {
        this.rut = rut;
    }

    public String getPorcentaje() {
        return porcentaje;
    }

    public void setPorcentaje(String porcentaje) {
        this.porcentaje = porcentaje;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (rut != null ? rut.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Tercerretiro)) {
            return false;
        }
        Tercerretiro other = (Tercerretiro) object;
        if ((this.rut == null && other.rut != null) || (this.rut != null && !this.rut.equals(other.rut))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "root.apiresttercerretiro.entity.Tercerretiro[ rut=" + rut + " ]";
    }
    
}
