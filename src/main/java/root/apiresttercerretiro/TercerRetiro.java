/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package root.apiresttercerretiro;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import root.apiresttercerretiro.dao.TercerretiroJpaController;
import root.apiresttercerretiro.dao.exceptions.NonexistentEntityException;
import root.apiresttercerretiro.entity.Tercerretiro;

/**
 *
 * @author ccruces
 */
@Path("retiro")
public class TercerRetiro {

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response listarSolicitudes() {

        /* Tercerretiro tr1=new Tercerretiro();
    tr1.setRut("19");
    tr1.setPorcentaje("5");
    Tercerretiro tr2=new Tercerretiro();
    tr2.setRut("28");
    tr2.setPorcentaje("10");
    
    List<Tercerretiro> lista=new ArrayList<Tercerretiro>();
    
   lista.add(tr1);
   lista.add(tr2);*/
        TercerretiroJpaController dao = new TercerretiroJpaController();

        List<Tercerretiro> lista = dao.findTercerretiroEntities();

        return Response.ok(200).entity(lista).build();

    }

    @GET
    @Path("/{idbuscar}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response consultarPorRut(@PathParam("idbuscar") String idbusca) {

        TercerretiroJpaController dao = new TercerretiroJpaController();

        Tercerretiro tercerretiro = dao.findTercerretiro(idbusca);
       System.out.print("rut devuelto"+tercerretiro.getRut());
        return Response.ok(200).entity(tercerretiro).build();

    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public Response add(Tercerretiro tercerRetiro) {

        try {
            TercerretiroJpaController dao = new TercerretiroJpaController();
            dao.create(tercerRetiro);
        } catch (Exception ex) {
            Logger.getLogger(TercerRetiro.class.getName()).log(Level.SEVERE, null, ex);
        }

        return Response.ok(200).entity(tercerRetiro).build();

    }

    @DELETE
    @Path("/{iddelete}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response delete(@PathParam("iddelete") String iddelete) {

        try {
            TercerretiroJpaController dao = new TercerretiroJpaController();

            dao.destroy(iddelete);
        } catch (NonexistentEntityException ex) {
            Logger.getLogger(TercerRetiro.class.getName()).log(Level.SEVERE, null, ex);
        }

        return Response.ok("cliente eliminado").build();
    }

    @PUT
    public Response update(Tercerretiro tercerRetiro) {

        try {
            TercerretiroJpaController dao = new TercerretiroJpaController();
            dao.edit(tercerRetiro);
        } catch (Exception ex) {
            Logger.getLogger(TercerRetiro.class.getName()).log(Level.SEVERE, null, ex);
        }

        return Response.ok(200).entity(tercerRetiro).build();
    }

}
