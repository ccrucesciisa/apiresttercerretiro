/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package root.apiresttercerretiro.dao;

import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.Persistence;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import root.apiresttercerretiro.dao.exceptions.NonexistentEntityException;
import root.apiresttercerretiro.dao.exceptions.PreexistingEntityException;
import root.apiresttercerretiro.entity.Tercerretiro;

/**
 *
 * @author CCRUCES
 */
public class TercerretiroJpaController implements Serializable {

    public TercerretiroJpaController() {
 
    }
    private EntityManagerFactory emf = Persistence.createEntityManagerFactory("clientes_PU");

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Tercerretiro tercerretiro) throws PreexistingEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(tercerretiro);
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findTercerretiro(tercerretiro.getRut()) != null) {
                throw new PreexistingEntityException("Tercerretiro " + tercerretiro + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Tercerretiro tercerretiro) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            tercerretiro = em.merge(tercerretiro);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                String id = tercerretiro.getRut();
                if (findTercerretiro(id) == null) {
                    throw new NonexistentEntityException("The tercerretiro with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(String id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Tercerretiro tercerretiro;
            try {
                tercerretiro = em.getReference(Tercerretiro.class, id);
                tercerretiro.getRut();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The tercerretiro with id " + id + " no longer exists.", enfe);
            }
            em.remove(tercerretiro);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Tercerretiro> findTercerretiroEntities() {
        return findTercerretiroEntities(true, -1, -1);
    }

    public List<Tercerretiro> findTercerretiroEntities(int maxResults, int firstResult) {
        return findTercerretiroEntities(false, maxResults, firstResult);
    }

    private List<Tercerretiro> findTercerretiroEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Tercerretiro.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Tercerretiro findTercerretiro(String id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Tercerretiro.class, id);
        } finally {
            em.close();
        }
    }

    public int getTercerretiroCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Tercerretiro> rt = cq.from(Tercerretiro.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
