<%-- 
    Document   : index
    Created on : 13-04-2021, 21:25:34
    Author     : ccruces
--%>


<%@page import="java.util.Iterator"%>
<%@page import="java.util.List"%>
<%@page import="root.apiresttercerretiro.entity.Tercerretiro"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%
      List<Tercerretiro> lista   = (List<Tercerretiro>) request.getAttribute("lista");
    Iterator<Tercerretiro> itTercerretiro = lista.iterator();
%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <form  name="form" action="TercerRetiroController" method="POST">
           <div class="cover-container d-flex h-100 p-3 mx-auto flex-column">
            
               
               <table border="1">
                    <thead>
                    <th>Rut</th>
                    <th>Porcentaje </th>
             
                    <th> </th>
        
                    </thead>
                    <tbody>
                        <%while (itTercerretiro.hasNext()) {
                       Tercerretiro per = itTercerretiro.next();%>
                        <tr>
                            <td><%= per.getRut()%></td>
                            <td><%= per.getPorcentaje()%></td>
                          
                 <td> <input type="radio" name="seleccion" value="<%= per.getRut()%>"> </td>
                
                        </tr>
                        <%}%>                
                    </tbody>           
                </table>
              <button type="submit" name="accion" value="ver" class="btn btn-success">ver solicitud</button>
            <button type="submit" name="accion" value="eliminar" class="btn btn-success">eliminar</button>

        
        </form>
    </body>
</html>
